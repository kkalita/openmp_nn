from __future__ import print_function

import time
import random
import numpy as np

class Network(object):
    def __init__(self, layers):
        self.layers = layers
        self.num_layers = len(layers)
        self.biases = [np.random.randn(y, 1) for y in self.layers[1:]]
        self.weights = [np.random.randn(y, x) / np.sqrt(x) for x, y in zip(self.layers[:-1], self.layers[1:])]

    def feedforward(self, x):
        for b, w in zip(self.biases, self.weights):
            out = sigmoid(np.dot(w, x) + b)
            x = out
        return out

    def train(self, data, epochs, samples_num, eta, lmbda=0.0):
        n = len(data)
        for epoch in range(epochs):
            random.shuffle(data)
            samples = []
            for k in range(0, n, samples_num):
                samples.append(data[k: k + samples_num])

            for sample in samples:
                self.update_sample(sample, eta, lmbda, n)

            accuracy = self.accuracy(data)
            print("{}: Accuracy on training data: {} / {}".format(epoch, accuracy, n))


    def update_sample(self, sample, eta, lmbda, n):
        grad_bs = [np.zeros(b.shape) for b in self.biases]
        grad_ws = [np.zeros(w.shape) for w in self.weights]
        for x, y in sample:
            delta_bs, delta_ws = self.backpropagation(x, y)
            grad_bs = [b + del_b for b, del_b in zip(grad_bs, delta_bs)]
            grad_ws = [w + del_w for w, del_w in zip(grad_ws, delta_ws)]
        self.biases = [ b - (eta / len(sample)) * gb for b, gb in zip(self.biases, grad_bs)]
        self.weights = [(1 - (eta * lmbda / n)) * w - (eta / len(sample)) * gw
                        for w, gw in zip(self.weights, grad_ws)]

    def backpropagation(self, x, y):
        grad_bs = [np.zeros(b.shape) for b in self.biases]
        grad_ws = [np.zeros(w.shape) for w in self.weights]

        activation = x
        activations = [x]
        zs = []

        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)

        delta = activations[-1] - y
        grad_bs[-1] = delta
        grad_ws[-1] = np.dot(delta, activations[-2].transpose())

        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l + 1].transpose(), delta) * sp
            grad_bs[-l] = delta
            grad_ws[-l] = np.dot(delta, activations[-l - 1].transpose())

        return (grad_bs, grad_ws)

    def accuracy(self, data):
        results = [(np.argmax(self.feedforward(x)), np.argmax(y))
                       for (x, y) in data]
        return sum(int(x == y) for (x, y) in results)

def sigmoid(z):
    return 1.0 / (1.0 + np.exp(-z))

def sigmoid_prime(z):
    return sigmoid(z) * (1 - sigmoid(z))
