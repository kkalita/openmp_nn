import mnist_loader
import network
import time

training_data, validation_data, test_data = mnist_loader.load_data()
net = network.Network([784, 30, 10])
start = time.time()
net.train(training_data, 10, 10, 3.0)
print time.time() - start
