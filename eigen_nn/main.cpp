#include <iostream>
#include <Eigen/Dense>
#include <fstream>
#include <cmath>
#include <chrono>

using Eigen::MatrixXd;
using Eigen::VectorXd;

struct Data {
    VectorXd input;
    VectorXd label;
};

typedef std::vector<Data> Batch;

int byteSwapInt(int i) {
    return ((i & 0x000000FF) << 24)
           + ((i & 0x0000FF00) << 8)
           + ((i & 0x00FF0000) >> 8)
           + ((i & 0xFF000000)  >> 24);
}

void ReadImages(std::vector<Data> &arr) {
    std::ifstream file("../data/train-images.idx3-ubyte", std::ios::binary);
    if (file.is_open()) {
        int magic_number = 0;
        int n_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char *) &magic_number, sizeof(magic_number));
        magic_number = byteSwapInt(magic_number);
        file.read((char *) &n_images, sizeof(n_images));
        n_images = byteSwapInt(n_images);
        file.read((char *) &n_rows, sizeof(n_rows));
        n_rows = byteSwapInt(n_rows);
        file.read((char *) &n_cols, sizeof(n_cols));
        n_cols = byteSwapInt(n_cols);
        arr.resize(n_images);

        for (int i = 0; i < n_images; ++i) {
            VectorXd vector = VectorXd::Zero(n_rows * n_cols);
            for (int r = 0; r < n_rows; ++r) {
                for (int c = 0; c < n_cols; ++c) {
                    unsigned char temp = 0;
                    file.read((char *) &temp, sizeof(temp));
                    vector((n_rows * r) + c) = ((double) temp / 255.0);
                }
            }
            arr[i].input = vector;
        }
    }
}
void ReadResults(std::vector<Data> & arr) {

    std::ifstream file("../data/train-labels.idx1-ubyte", std::ios::binary);
    if (file.is_open()) {
        int magic_number = 0;
        int n_images = 0;
        file.read((char *) &magic_number, sizeof(magic_number));
        magic_number = byteSwapInt(magic_number);
        file.read((char *) &n_images, sizeof(n_images));
        n_images = byteSwapInt(n_images);
        arr.resize(n_images);

        for (int i = 0; i < n_images; ++i) {
            VectorXd vector = VectorXd::Zero(10);
            unsigned char temp = 0;
            file.read((char *) &temp, sizeof(temp));
            int res = (int) temp;
            vector(res) = 1;
            arr[i].label = vector;
        }
    }
}

VectorXd exponent(VectorXd x) {
    VectorXd v;
    v = VectorXd::Zero(x.size(), 1);
    for (int i = 0; i < x.size(); i++) {
        v(i) = std::exp(x(i));
    }
    return v;
}

class Network {

    std::vector<int> layers;
    std::vector<VectorXd> biases;
    std::vector<MatrixXd> weights;
    std::vector<Batch> miniBatches;
    long layersSize;

    void initializeBiases() {
        std::default_random_engine gen;
        std::normal_distribution<double> gaussian(0.0, 1.0);
        biases.resize(layers.size() - 1);
        for (int i = 0; i < biases.size(); i++) {
            biases[i] = VectorXd::Zero(layers[i + 1], 1);
        }

        for (auto& bias : biases) {
            for (int i = 0; i < bias.size(); i++) {
                bias(i) = gaussian(gen);
            }
        }
    }

    void initializeWeights() {
        std::default_random_engine gen;
        std::normal_distribution<double> gaussian(0.0, 1.0);
        weights.resize((layers.size() - 1));
        for (int i = 0; i < weights.size(); i++) {
            weights[i] = MatrixXd::Zero(layers[i+1], layers[i]);
        }

        for (auto& matrix : weights) {
            for (int i = 0; i < matrix.rows(); i++) {
                for (int j = 0; j < matrix.cols(); j++) {
                    matrix(i, j) = gaussian(gen) / std::sqrt(matrix.rows());
                }
            }
        }
    }

    void updateMiniBatch(Batch & miniBatch, double eta, double lambda, unsigned long n) {
        auto gradientBiases = std::vector<VectorXd>();
        for (auto& bias : biases) {
            gradientBiases.push_back(VectorXd::Zero(bias.rows(), 1));
        }
        auto gradientWeights = std::vector<MatrixXd>();
        for (auto& weight : weights) {
            gradientWeights.push_back(MatrixXd::Zero(weight.rows(), weight.cols()));
        }

        for (auto& data : miniBatch) {
            auto deltaBiases = std::vector<VectorXd>(biases.size());
            auto deltaWeights = std::vector<MatrixXd>(weights.size());
            backPropagation(data, deltaBiases, deltaWeights);

            for (int i = 0; i < gradientBiases.size(); i++) {
                gradientBiases[i] = gradientBiases[i] + deltaBiases[i];
                gradientWeights[i] = gradientWeights[i] + deltaWeights[i];
            }
        }

        for (int i = 0; i < this->biases.size(); i++) {
            biases[i] = biases[i] - (eta / miniBatch.size()) * gradientBiases[i];
            weights[i] = (1 - (eta * lambda / n)) * weights[i] - (eta / miniBatch.size()) * gradientWeights[i];
        }

    }

    void backPropagation(Data & data, std::vector<VectorXd> & deltaBiases, std::vector<MatrixXd> & deltaWeights ) {
        auto x = data.input;
        auto xs = std::vector<VectorXd>();
        auto zs = std::vector<VectorXd>();
        xs.push_back(x);
        for (int i = 0; i < layersSize - 1; i++) {
            auto& b = biases[i];
            auto& w = weights[i];
            auto tmp = w * x;
            auto z = tmp + b;

            zs.push_back(z);
            x = sigmoid(z);
            xs.push_back(x);
        }

        VectorXd delta = xs[xs.size() - 1] - data.label;
        long size = deltaBiases.size();
        deltaBiases[size - 1] = delta;
        deltaWeights[size - 1] = delta * xs[xs.size() - 2].transpose();

        for (long i = 2; i < layersSize; i++) {
            auto& w = weights[weights.size() -i + 1];
            auto& z = zs[zs.size() - i];
            auto sp = sigmoid_prime(z);
            delta = ((w.transpose() * delta).array() * sp.array()).matrix();
            deltaBiases[size - i] = delta;
            deltaWeights[size - i] = delta * xs[xs.size() - i - 1].transpose();
        }
    }

    VectorXd sigmoid(VectorXd z) {
        return (1.0 / (1.0 + exponent(-z).array())).matrix();
    }

    VectorXd sigmoid_prime(VectorXd z) {
        return (sigmoid(z).array() * (1.0 - sigmoid(z).array())).matrix() ;
    }

    VectorXd feedforward(VectorXd x) {
        for (int i = 0; i < layersSize - 1; i++) {
            auto tmp = weights[i] * x;
            auto z = tmp + biases[i];
            x = sigmoid(z);
        }
        return x;
    }

    int argmax(VectorXd & v) {
        int max = 0;
        for (int i = 0; i < v.size(); i++) {
            if (v(i) > v(max)) {
                max = i;
            }
        }
        return max;
    }

    int accuracy(std::vector<Data> & dataSet) {
        int correct = 0;
        for (int i = 0; i < dataSet.size(); i++) {
            auto output = this->feedforward(dataSet[i].input);
            if (argmax(output) == argmax(dataSet[i].label)) {
                correct++;
            }
        }
        return correct;
    }

public:
    Network(std::vector<int> layers) {
        this->layers = layers;
        this->layersSize = layers.size();
        initializeBiases();
        initializeWeights();
    }

    void train(std::vector<Data> & data, int epochs, int sampleSize, double eta, double lambda = 0.0) {
        for (int k = 0; k < epochs; k++) {
            miniBatches = std::vector<Batch>((unsigned long) (data.size()) / sampleSize);
            for (int i = 0; i < data.size() / sampleSize; i++) {
                for (int j = 0; j < sampleSize; j++) {
                    miniBatches[i].push_back(data[i * sampleSize + j]);
                }
            }
            for (auto miniBatch : miniBatches) {
                updateMiniBatch(miniBatch, eta, lambda, data.size());
            }
            std::cout << "Epoch " << k <<": Accuracy: " << accuracy(data) << "/" << data.size() << std::endl;
        }
    }
};

int main() {
    auto v = std::vector<Data>();

    ReadImages(v);
    ReadResults(v);
    v.resize(1000);
    std::vector<int> layers(3);
    layers[0] = 784;
    layers[1] = 30;
    layers[2] = 10;

    Network nn(layers);

    auto start = std::chrono::steady_clock::now();
    nn.train(v, 10, 10, 3.0);
    std::chrono::duration<double> time = std::chrono::steady_clock::now() - start;
    std::cout << time.count() << std::endl;
    return 0;
}
