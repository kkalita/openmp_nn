#ifndef CUDASIMPLE_MATRIX_OPS_H
#define CUDASIMPLE_MATRIX_OPS_H

#include <vector>
#include <assert.h>

typedef std::vector<double> Vector;
typedef std::vector<Vector> Matrix;

Matrix transpose(const Matrix & a) {
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(cols, Vector(rows));
#pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[j][i] = a[i][j];
        }
    }
    return m;
}

Matrix m_add(const Matrix &a, const Matrix & b) {
    assert(a.size() == b.size() && a[0].size() == b[0].size());
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(rows, Vector(cols));
#pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[i][j] = a[i][j] + b[i][j];
        }
    }
    return m;
}
Matrix m_sub(const Matrix &a, const Matrix &b) {
    assert(a.size() == b.size() && a[0].size() == b[0].size());
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(rows, Vector(cols));
#pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[i][j] = a[i][j] - b[i][j];
        }
    }
    return m;
}
Matrix m_mul(const Matrix & a, const Matrix & b) {
    unsigned long a_rows = a.size(), a_cols = a[0].size();
    unsigned long b_rows = b.size(), b_cols = b[0].size();
    assert(a_cols == b_rows);
    auto b_T = transpose(b);
    Matrix m(a_rows, Vector(b_cols, 0));
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < a_rows; i++) {
        for (int j = 0; j < b_cols; j++) {
            double tmp = 0;
            for (int k = 0; k < b_rows; k++ ) {
                tmp += a[i][k] * b_T[j][k];
            }
            m[i][j] = tmp;
        }
    }
    return m;
}

Matrix m_had(const Matrix & a, const Matrix & b) {
    assert(a.size() == b.size() && a[0].size() == b[0].size());
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(rows, Vector(cols));
#pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[i][j] = a[i][j] * b[i][j];
        }
    }
    return m;
}


Matrix sm_mul(double d, const Matrix & a) {
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(rows, Vector(cols));
#pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[i][j] = d * a[i][j];
        }
    }
    return m;
}

Matrix sm_sub(double d, const Matrix & a) {
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(rows, Vector(cols));
    int i , j;
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[i][j] = d - a[i][j];
        }
    }
    return m;
}

Matrix sigmoid(const Matrix & a) {
    unsigned long long rows = a.size(), cols = a[0].size();
    Matrix m(rows, Vector(cols));
    int i , j;
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            m[i][j] = 1.0 / (1.0 + std::exp(-a[i][j]));
        }
    }
    return m;
}

Matrix sigmoid_prime(const Matrix & a) {
    auto sig = sigmoid(a);
    return m_had(sig, sm_sub(1.0, sig));
}
#endif
