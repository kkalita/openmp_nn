#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>

#include "matrix_ops.h"

int byteSwapInt(int i) {
    return ((i & 0x000000FF) << 24)
           + ((i & 0x0000FF00) << 8)
           + ((i & 0x00FF0000) >> 8)
           + ((i & 0xFF000000)  >> 24);
}

struct Data {
    Matrix input;
    Matrix label;
};

typedef std::vector<Data*> Batch;

void print(Matrix m) {
    for (int i = 0; i < m.size(); i++) {
        for (int  j = 0; j < m[0].size(); j++) {
            printf("%.6f ", m[i][j]);
        }
        printf("\n");
    }
}

void ReadImages(std::vector<Data> &arr, int max = 0) {
    std::ifstream file("../data/train-images.idx3-ubyte", std::ios::binary);

    if (file.is_open()) {
        int magic_number = 0;
        int n_images = 0;
        int n_rows = 0;
        int n_cols = 0;
        file.read((char *) &magic_number, sizeof(magic_number));
        magic_number = byteSwapInt(magic_number);
        file.read((char *) &n_images, sizeof(n_images));
        n_images = byteSwapInt(n_images);
        file.read((char *) &n_rows, sizeof(n_rows));
        n_rows = byteSwapInt(n_rows);
        file.read((char *) &n_cols, sizeof(n_cols));
        n_cols = byteSwapInt(n_cols);

        n_images = 1000;
        arr.resize(n_images);

        for (int i = 0; i < n_images; ++i) {
            Matrix input(n_rows * n_cols, Vector(1));
            for (int r = 0; r < n_rows; ++r) {
                for (int c = 0; c < n_cols; ++c) {
                    unsigned char temp = 0;
                    file.read((char *) &temp, sizeof(temp));
                    input[(n_rows * r) + c][0] = ((double) temp / 255.0);
                }
            }
            arr[i].input = input;
        }
    }
}

void ReadResults(std::vector<Data> & arr) {
    std::ifstream file("../data/train-labels.idx1-ubyte", std::ios::binary);
    if (file.is_open()) {
        int magic_number = 0;
        int n_images = 0;
        file.read((char *) &magic_number, sizeof(magic_number));
        magic_number = byteSwapInt(magic_number);
        file.read((char *) &n_images, sizeof(n_images));
        n_images = byteSwapInt(n_images);

        arr.resize(n_images);

        for (int i = 0; i < n_images; ++i) {
            Matrix label(10, Vector(1));
            unsigned char temp = 0;
            file.read((char *) &temp, sizeof(temp));
            int res = (int) temp;
            label[res][0] = 1;
            arr[i].label = label;
        }
    }
}

struct Network {
    std::vector<unsigned long> layers;
    std::vector<Matrix> biases;
    std::vector<Matrix> weights;
    std::vector<Batch> batches;

    void initializeBiases() {
        std::default_random_engine gen;
        std::normal_distribution<double> gaussian(0.0, 1.0);
        biases.resize(layers.size() - 1);
        for (int i = 0; i < biases.size(); i++) {
            biases[i] = Matrix(layers[i + 1], Vector(1));
        }

        for (auto& bias : biases) {
            for (int i = 0; i < bias.size(); i++) {
                bias[i][0] = gaussian(gen);
            }
        }
    }

    void initializeWeights() {
        std::default_random_engine gen;
        std::normal_distribution<double> gaussian(0.0, 1.0);
        weights.resize((layers.size() - 1));
        for (int i = 0; i < weights.size(); i++) {
            weights[i] = Matrix(layers[i+1], Vector(layers[i]));
        }

        for (auto& weight : weights) {
            for (int i = 0; i < weight.size(); i++) {
                for (int j = 0; j < weight[i].size(); j++) {
                    weight[i][j] = gaussian(gen) / std::sqrt(weight[i].size());
                }
            }
        }
    }

    void train(std::vector<Data> & data, int epochs, unsigned long batchSize, double eta, double lambda = 0.0) {
        for (int k = 0; k < epochs; k++) {
            std::random_shuffle(data.begin(), data.end());

            batches = std::vector<Batch>(data.size() / batchSize);
            for (int i = 0; i < data.size() / batchSize; i++) {
                for (int j = 0; j < batchSize; j++) {
                    batches[i].push_back(&data[i * batchSize + j]);
                }
            }
            for (auto& batch : batches) {
                updateMiniBatch(batch, eta, lambda, data.size());
            }

            printf("%d: Accuracy: %d / %lu\n", k, accuracy(data), data.size());
        }
    }

    void updateMiniBatch(Batch &batch, double eta, double lambda, unsigned long n) {
        auto gradientBiases = std::vector<Matrix>(biases.size());
        for (int i = 0; i < biases.size(); i++) {
            gradientBiases[i] = Matrix(biases[i].size(), Vector(1));
        }
        auto gradientWeights = std::vector<Matrix>(weights.size());
        for (int i = 0; i < weights.size(); i++) {
            gradientWeights[i] = Matrix(layers[i+1], Vector(layers[i]));
        }

        for (auto& data : batch) {
            auto deltaBiases = std::vector<Matrix>(biases.size());
            auto deltaWeights = std::vector<Matrix>(weights.size());
            backPropagation(data, deltaBiases, deltaWeights);

            for (int i = 0; i < layers.size() - 1; i++) {
                gradientBiases[i] = m_add(gradientBiases[i], deltaBiases[i]);
                gradientWeights[i] = m_add(gradientWeights[i], deltaWeights[i]);
            }
        }

        for (int i = 0; i < layers.size() - 1; i++) {
            // b = b - (eta / batch.size()) * grad_b
            biases[i] = m_sub(biases[i], sm_mul(eta / batch.size(), gradientBiases[i]));
            // (1 - (eta * lambda / n) * w - (eta / batch.size()) * grad_w
            weights[i] = m_sub(sm_mul((1 - (eta * lambda / n)), weights[i]), sm_mul((eta / batch.size()), gradientWeights[i]));
        }
    }

    void backPropagation(Data * data, std::vector<Matrix> & deltaBiases, std::vector<Matrix> & deltaWeights ) {
        auto x = data->input;
        auto xs = std::vector<Matrix>();
        auto zs = std::vector<Matrix>();
        xs.push_back(x);
        for (int i = 0; i < layers.size() - 1; i++) {
            auto& b = this->biases[i];
            auto& w = this->weights[i];

            auto tmp = m_mul(w, x);
            auto z = m_add(tmp, b);

            zs.push_back(z);
            x = sigmoid(z);
            xs.push_back(x);
        }

        auto delta = m_sub(xs[xs.size() - 1], data->label);
        long size = deltaBiases.size();

        deltaBiases[size - 1] = delta;
        deltaWeights[size - 1] = m_mul(delta, transpose(xs[xs.size() - 2]));

        for (long i = 2; i < layers.size(); i++) {
            auto& w = this->weights[this->weights.size() -i + 1];
            auto& z = zs[zs.size() - i];
            auto sp = sigmoid_prime(z);
            delta = m_had(m_mul(transpose(w), delta), sp);
            deltaBiases[size - i] = delta;
            deltaWeights[size - i] = m_mul(delta, transpose(xs[xs.size() - i - 1]));
        }
    }

    Matrix feedforward(Matrix x) {
        for (int i = 0; i < layers.size() - 1; i++) {
            auto z = m_add(m_mul(weights[i], x), biases[i]);
            x = sigmoid(z);
        }
        return x;
    }

    int argmax(Matrix m) {
        int max = 0;
        for (int i = 0; i < m.size(); i++) {
            if (m[i][0] > m[max][0]) {
                max = i;
            }
        }
        return max;
    }

    int accuracy(std::vector<Data> & dataSet) {
        int correct = 0;
        for (int i = 0; i < dataSet.size(); i++) {
            auto output = this->feedforward(dataSet[i].input);
            if (argmax(output) == argmax(dataSet[i].label)) {
                correct++;
            }
        }
        return correct;
    }

    Network(std::vector<unsigned long> layers) {
        this->layers = layers;
        initializeBiases();
        initializeWeights();
    }
};

int main() {
    auto v = std::vector<Data>();
    int n = 1000;
    ReadImages(v);
    ReadResults(v);

    v.resize(n);
    std::vector<unsigned long> layers(3);
    layers[0] = 784;
    layers[1] = 100;
    layers[2] = 10;

    Network nn(layers);

    auto start = std::chrono::steady_clock::now();
    nn.train(v, 10, 10, 3.0);
    std::chrono::duration<double> time = std::chrono::steady_clock::now() - start;
    printf("TIME: %.6f\n", time.count());
    return 0;
}
